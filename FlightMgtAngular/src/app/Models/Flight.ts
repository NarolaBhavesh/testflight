export class Flight {
  ID: number;
  FlightNo: string;
  StartDate: string;
  EndDate: string;
  PassCapacity: number;
  DepartCity: string;
  ArrivalCity: string;
  NoOfPassenger: number;
  Status: string;
}
